import json

import aiohttp

from backend import const
from backend.const import headers, URL, body, commands_dict
from telegram_bot.database import Database

db = Database("my_super_db.db")


def register_command(func):
    commands_dict[func.__name__] = func
    return func


async def request_json(headers: dict, body: dict, url: str):
    """
    Получаем json банкоматов
    """""
    async with aiohttp.ClientSession() as session:
        async with session.post(
                url=url,
                headers=headers,
                data=json.dumps(body),
                ssl=False
        ) as resp:
            html = await resp.json()
            # print(json.dumps(html, indent=4, sort_keys=False))
            return html


async def refresh_machine(json: dict, diction: {str: int}):
    """
    Получаем актуальный словарь из банкоматов
    """
    # Будет хранить все адреса, которые получит в json, чтобы потом их сверить с текущими
    check: list[str] = []
    # Список с банкоматами
    inf = ''
    try:
        machine: list[dict] = [json['payload']['clusters'][k]['points'] for k in
                               range(len(json['payload']['clusters']))]
    except IndexError:
        return diction, 'Изменений нет'

    for cash_machine in machine:

        # Строчка 10/10
        cash_machine = cash_machine[0]

        address = cash_machine['address']
        check.append(address)
        amount = cash_machine['limits'][0]['amount']
        if address not in diction:
            print(f'Появился банкомат по адерсу {address} с суммой {amount}')
            inf += f'Появился банкомат по адерсу {address} с суммой {amount}$\n\n'
            diction[address] = amount
        else:
            if diction[address] != amount:
                print(f'Обновился банкомат по адерсу {address} с суммой {amount}')
                inf += f'Обновился банкомат по адерсу {address} с суммой {amount}$\n\n'
                diction[address] = amount

    for key in diction.copy():
        if key not in check:
            diction.pop(key)

    return inf + "\n", diction


@register_command
async def get_info(diction=const.diction):
    jsn: dict = await request_json(headers=headers, body=body, url=URL)
    info, diction = await refresh_machine(json=jsn, diction=diction)
    return info


@register_command
async def get_statistics():
    more_often, max_amout, avg = db.get_statistics()
    more_often, count_appearances = more_often
    max_addr, max_amout, max_time = max_amout
    result = f"Чаще всего деньги появляются по адресу {more_often}\n" \
             f"Количество появлений: {count_appearances}\n\n" \
             f"Максимальная сумма последний раз в банкомате была по адресу {max_addr}\n" \
             f"Сумма: {max_amout}$\n" \
             f"Время: {max_time}\n\n" \
             f"Всего в среднем было {round(avg[0])}$ в банкоматах\n\n"
    return result

