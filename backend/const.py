URL = 'https://api.tinkoff.ru/geo/withdraw/clusters'
headers = {
    'accept': '*/*',
    'content-type': 'application/json',
    'origin': 'https://www.tinkoff.ru',
    'referer': 'https://www.tinkoff.ru',
    'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="98", "Google Chrome";v="98"',
    'sec-ch-ua-platform': 'Windows',
    'sec-fetch-site': 'same-site',
    'sec-fetch-mode': 'cors',
    'sec-fetch-dest': 'empty',
    'sec-ch-ua-mobile': '?0',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36'
}

body = {
    "bounds": {
        "bottomLeft": {
            "lat": 55.7236091318682,
            "lng": 49.022215991923574
        },
        "topRight": {
            "lat": 55.88147196777564,
            "lng": 49.31866966453659
        }
    },
    "filters": {
        "showUnavailable": True,
        "currencies": ["USD"]
    },
    "zoom": 12
}

'''
FOR MOSCOW
body = {
    "bounds": {"bottomLeft": {"lat": 55.3625542025249, "lng": 36.83953288358896},
               "topRight": {"lat": 56.090267408180075, "lng": 38.72506144804206}},
    "filters": {
        "showUnavailable": True,
        "currencies": ["USD"]
    },
    "zoom": 10
}
'''

diction = {}
commands_dict = {}
