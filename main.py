import uvicorn
import websockets
from fastapi import FastAPI, WebSocket
from fastapi.responses import HTMLResponse
from fastapi_utils.tasks import repeat_every
from starlette.websockets import WebSocketDisconnect

from backend import const
from backend.classs import manager
from backend.const import commands_dict, headers, body, URL
from backend.services import request_json, refresh_machine, db, get_info

app = FastAPI()

with open("./js_page/index.html") as file:
    html = file.read()


@app.on_event("startup")
@repeat_every(seconds=30)
async def websocket_cash_machine():
    """
    Функция для использования нашего API на FastAPI
    """
    try:
        jsn: dict = await request_json(headers=headers, body=body, url=URL)
        info, diction = await refresh_machine(json=jsn, diction=const.diction)
        for addr, amount in diction.items():
            db.add_atm(addr, amount)
        if info:
            for inf in info.split('\n\n'):
                if inf:
                    await manager.broadcast(inf)
    except WebSocketDisconnect:
        pass


@app.get("/")
async def get():
    return HTMLResponse(html)


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await manager.connect(websocket)
    start_info = await get_info(diction={})
    for inf in start_info.split('\n\n'):
        if inf:
            await manager.send_personal_message(inf, websocket)
    while True:
        try:
            data = await websocket.receive_text()
            print(data)
            info = await commands_dict[data]()
            if info:
                await manager.send_personal_message(str(info), websocket)
        except KeyError:
            await manager.send_personal_message("Нет такой команды")
        except websockets.ConnectionClosed:
            manager.disconnect(websocket)


if __name__ == '__main__':
    uvicorn.run("main:app", port=8000, host="127.0.0.1", reload=True)
