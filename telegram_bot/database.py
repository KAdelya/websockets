import sqlite3
import datetime


class Database:
    def __init__(self, name: str):
        self.connect = sqlite3.connect(name)
        self.cursor = self.connect.cursor()
        try:
            self.cursor.execute('''CREATE TABLE users (user_id integer primary key)''')
            self.cursor.execute('''CREATE TABLE atm (address text, amount integer, date timestamp)''')
            self.connect.commit()
        except sqlite3.OperationalError:
            pass

    def add_user(self, user_id: int):
        with self.connect:
            try:
                self.cursor.execute(f"INSERT INTO 'users' VALUES ({user_id})")
                self.connect.commit()
            except sqlite3.IntegrityError:
                return True

    def add_atm(self, address, amount):
        with self.connect:
            self.cursor.execute(f"INSERT INTO 'atm' VALUES (?, ? ,?)", (address, amount, datetime.datetime.now()))
            self.connect.commit()

    def give_users(self):
        return self.cursor.execute(f"SELECT * FROM 'users'").fetchall()

    def get_statistics(self):
        more_often = self.cursor.execute(f"SELECT address, COUNT(address) "
                                         f"FROM 'atm' GROUP BY address ORDER BY COUNT(*) DESC LIMIT 1").fetchone()
        max_amout = self.cursor.execute(f"SELECT address, amount, date FROM 'atm' ORDER BY amount DESC, date DESC").fetchone()
        avg = self.cursor.execute(f"SELECT avg(amount) FROM 'atm'").fetchone()
        return more_often, max_amout, avg
