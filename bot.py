import websockets
from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor

from telegram_bot.token_bot import TOKEN, admin_id
from telegram_bot.database import Database

db = Database("my_super_db.db")
bot = Bot(token=TOKEN)
dp = Dispatcher(bot)

start_message = "Этот бот будет присылать вам актуальную информацию об наличий долларов в банкоматах Тинькофф " \
                "на территорий Казани"
message_for_new_user = 'Вы подписались на бота, ' \
                       'теперь он будет присылать вам сообщения об наличий долларов в банкоматах, ' \
                       'следующее сообщение будет в течение 5 минут'


@dp.message_handler(commands=['start'])
async def command_start(message: types.Message):
    await bot.send_message(message.from_user.id, f'Привет {message.from_user["first_name"]}, {start_message}')

    if not db.add_user(message.from_user.id):
        await bot.send_message(message.from_user.id, message_for_new_user)


@dp.message_handler(commands=["start_work"])
async def send_info(message: types.Message):
    if message.from_user.id != admin_id:
        await message.reply("У вас не прав на эту команду")
        return None

    async with websockets.connect("ws://127.0.0.1:8000/ws") as websocket:
        try:
            await bot.send_message(message.from_user.id, "start work")
            while True:
                data = await websocket.recv()
                result = ''
                while "\n" not in data:
                    result += data + "\n\n"
                    data = await websocket.recv()
                result += data

                if result:
                    await websocket.send('get_statistics')
                    data = await websocket.recv()
                    result += "Статистика:\n" + data
                    for user in db.give_users():
                        await bot.send_message(*user, result)

        except websockets.ConnectionClosed:
            pass


if __name__ == '__main__':
    executor.start_polling(dp)
